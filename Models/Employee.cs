using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_core_api_sample_0.Models
{
    public class Employee
    {        
        public int EmpNo { get; set; }       
        public string FirstName {get;set;}
        public string LastName {get; set;}
        public string Gender {get; set;}
        public DateTime BirthDate {get;set;}
        public DateTime HireDate{get;set;}
    }

}

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Entities 
{
    public class Employee 
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Column("emp_no")]
        public int EmpNo { get; set; }

        [Column("first_name")]
        public string FirstName {get;set;}

        [Column("last_name")]
        public string LastName {get; set;}

        public string Gender {get; set;}

        [Column("birth_date")]
        public DateTime BirthDate {get;set;}

        [Column("hire_date")]
        public DateTime HireDate{get;set;}

    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Mvc;
using dotnet_core_api_sample_0.Services.Repositories;

namespace dotnet_core_api_sample_0.Controllers
{
    [Route("api/[controller]")]
    public class EmployeesController : Controller
    {
        IEmployeeRepository _repository;
        public EmployeesController(IEmployeeRepository repo) : base()
        {
            _repository = repo;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                Employee emp = _repository.GetEmployee(id);
                if (emp == null)
                {
                    return NotFound();
                }
                return Ok(emp);

            }
            catch (Exception)
            {
                return StatusCode(500);
            }
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody]Employee employee)
        {
            try
            {
                _repository.InsertNewEmployee(employee);
                _repository.SaveChanges();
                return Ok();
            }
            catch (Exception)
            {
                return StatusCode(500);
            }


        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Employee employee)
        {
            try
            {
                _repository.UpdateEmployee(id, employee);
                _repository.SaveChanges();
                return Ok();
            }
            catch (NotFoundException )
            {
                return NotFound();
            }
            catch (Exception )
            {
                return StatusCode(500);
            }
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _repository.DeleteEmployee(id);
                _repository.SaveChanges();
                return Ok();
            }
            catch (NotFoundException )
            {
                return NotFound();
            }
            catch (Exception )
            {
                return StatusCode(500);
            }
        }
    }
}

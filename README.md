# README #

### What is this repository for? ###

This repo comtains the Lunch and Learn .NET Core + webapi sample. 

### How do I get set up? ###

* This code requires the Employees Sample Database from MySQL it can be found on [mysql](https://dev.mysql.com/doc/employee/en/employees-installation.html) and on [github](https://github.com/datacharmer/test_db).

* In order to get the PUT to work correctly you will need to set the emp_no column in the Empoyees table to auto increment. (Otherwise your PUTs will just add a new record each time.) You can do this by runnin the following code 


```
#!sql

   SET FOREIGN_KEY_CHECKS=0;
   ALTER TABLE employees.employees MODIFY COLUMN emp_no INT auto_increment;
   SET FOREIGN_KEY_CHECKS=1;

```

* Setup the database user by running the following script (note this grants all privileges to the test user for convienence. Obvs' this is only for development purposes. )


```
#!sql

   CREATE USER 'apitest'@'localhost' IDENTIFIED BY 'Tester45TGFDE3';
   GRANT ALL PRIVILEGES ON *.* TO 'apitest'@'localhost';
```



* Run 


```
#!bash

   dotnet restore 
   dotnet run

```


### How to test it out ###

Here are some of the commands we demoed...


```
#!text

GET localhost:5000/api/employees/{id}

POST localhost:5000/api/employees with body 
   {
     "firstName": "Jason",
     "lastName": "Bourne",
     "gender": "M",
     "birthDate": "1954-05-01T00:00:00",
     "hireDate": "1986-12-01T00:00:00"
   }

PUT localhost:5000/api/employees/{id} with body 
   {
     "firstName": "Yason",
     "lastName": "Bourne",
     "gender": "M",
     "birthDate": "1954-05-01T00:00:00",
     "hireDate": "1986-12-01T00:00:00"
   }

 DELETE localhost:5000/api/employees/500000
```


### Who do I talk to? ###

dtorres
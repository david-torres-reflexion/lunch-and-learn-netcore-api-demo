using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_core_api_sample_0.Services.Repositories
{
    public class NotFoundException : System.Exception
    {
        public NotFoundException() { }
        public NotFoundException( string message ) : base( message ) { }
        public NotFoundException( string message, System.Exception inner ) : base( message, inner ) { }
    }

    
    public class EmployeeRepository : IEmployeeRepository
    {
        EmployeeContext _employeeContext;

        public EmployeeRepository(EmployeeContext context)
        {
            _employeeContext = context;
        }

        public Employee GetEmployee(int id)
        {
            Employee emp = _employeeContext.Employees.Where(u => u.EmpNo == id).FirstOrDefault();
            return emp;
        }

        public void InsertNewEmployee(Employee employee)
        {
            _employeeContext.Employees.Add(employee);
        }

        public void UpdateEmployee(int id, Employee employee)
        {
            Employee employeeInDb = _employeeContext.Employees.Where(e => e.EmpNo == id).FirstOrDefault();
            if( employeeInDb == null )
            {
                throw new NotFoundException(); 
            }            
            employeeInDb.FirstName = employee.FirstName;
            employeeInDb.LastName = employee.LastName;
            employeeInDb.Gender = employee.Gender;
            employeeInDb.BirthDate = employee.BirthDate;
            employeeInDb.HireDate = employee.HireDate;            
        }

        public void DeleteEmployee(int id )
        {
            Employee employeeInDb = _employeeContext.Employees.Where(e => e.EmpNo == id).FirstOrDefault();
            if( employeeInDb == null )
            {
                throw new NotFoundException(); 
            }   
            _employeeContext.Employees.Remove(employeeInDb);
        }


        public void SaveChanges()
        {
            _employeeContext.SaveChanges();
        }


    }
}

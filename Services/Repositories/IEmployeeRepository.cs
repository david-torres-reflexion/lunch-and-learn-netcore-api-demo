using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Entities;
using Microsoft.AspNetCore.Mvc;

namespace dotnet_core_api_sample_0.Services.Repositories
{
    public interface IEmployeeRepository
    {        
        Employee GetEmployee(int id);
        void InsertNewEmployee(Employee employee);
        void UpdateEmployee(int id, Employee employee);
        void DeleteEmployee(int id );
        void SaveChanges();
    }   
}
